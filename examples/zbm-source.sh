cd /root/zbm

# Backup VMs to zbm-dest
python3 zbm/zbm.py backup-pve \
  --remote zbm-dest \
  --prefix bak-zbm- \
  --local-zfs-root rpool/data \
  --remote-zfs-root main/bak-zbm \
  --remote-metadata-dir /root/backups_metadata \
  -y

