cd /root/zbm

# Replicate backups to zbm-replica
python3 zbm/zbm.py replicate \
  --remote zbm-replica \
  --remote-zfs-root main/bak-zbm \
  --local-zfs-root main/bak-zbm

# Prune local backups, ensuring we keep non-replicated backups,
# and a snapshot in common with the replica
python3 zbm/zbm.py prune \
  --local-zfs-root main/bak-zbm \
  --prefix bak-zbm- \
  --keep-last 2 \
  --keep-hourly 2 \
  --remote zbm-replica \
  --remote-zfs-root main/bak-zbm \
  -y

python3 zbm/zbm/zbm.py verify \
  --local-zfs-root main/bak-alphabet \
  --prefix bak-alphabet- \
  --max-age-days 1 \
  --min-snapshots 2

