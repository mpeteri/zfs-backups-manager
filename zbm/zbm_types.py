from dataclasses import dataclass
from typing import List, Optional, Set

from ordered_enum import OrderedEnum


@dataclass
class RemoteServer:
    hostname: str
    port: int = 22

    def ssh_args(self) -> List[str]:
        return ["-T", "-o", "BatchMode=yes", self.hostname, "-p", str(self.port)]


@dataclass
class VolumeBackupJob:
    remote_server: RemoteServer
    prefix: str
    local_zfs_root: str
    remote_zfs_root: str
    allow_encrypted: bool


@dataclass
class BackupReplicationJob(VolumeBackupJob):
    only_last: bool
    types: Set[str]
    is_encrypted: bool
    include_vms: Optional[List[int]] = None
    exclude_vms: Optional[List[int]] = None


class DangerLevel(OrderedEnum):
    NO_EFFECT = 1
    NORMAL = 2
    DANGEROUS = 3
    SUSPICIOUS = 4
    UNKNOWN = 5


NONINTERACTIVE = [False]
DRY_RUN = [False]
