# type: ignore
import os
import subprocess
from typing import Optional

import tqdm

from tools.processes import close_print_err, run


def create_key(keyfile: str) -> None:
    os.umask(0o077)
    run("dd", ["if=/dev/random", f"of={keyfile}", "bs=1", "count=32", "conv=excl"])


def print_key(keyfile: str) -> None:
    os.umask(0o077)
    print(run("hexdump", [keyfile]))


def encrypt_snapshot(
    source_with_snapshot: str,
    destination: str,
    keyfile: str,
    source_last_snap: Optional[str],
) -> None:
    if source_last_snap is None:
        zfs_send_args = ["zfs", "send", source_with_snapshot]
        zfs_recv_args = [
            "zfs",
            "recv",
            "-u",
            "-o",
            "encryption=on",
            "-o",
            "keyformat=raw",
            "-o",
            f"keylocation=file://{keyfile}",
            destination,
        ]
    else:
        zfs_send_args = [
            "zfs",
            "send",
            "-i",
            "@" + source_last_snap,
            source_with_snapshot,
        ]
        zfs_recv_args = ["zfs", "recv", "-u", destination]

    print((" ".join(zfs_send_args)) + " | " + (" ".join(zfs_recv_args)))

    zfs_send = subprocess.Popen(
        zfs_send_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    pbar = tqdm.tqdm(unit="B", unit_scale=True, unit_divisor=1024)
    zfs_recv = subprocess.Popen(
        zfs_recv_args,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    assert zfs_send.stdout is not None
    assert zfs_send.stdout is not None
    assert zfs_recv.stdin is not None
    try:
        i: int = 0
        while True:
            data: bytes = zfs_send.stdout.read(40960)
            if len(data) == 0:
                break
            zfs_recv.stdin.write(data)
            pbar.update(len(data))
            i += len(data)

        pbar.close()
        zfs_recv.stdin.close()
        print(f"End of loop after {i} bytes transferred")
    except BrokenPipeError:
        pbar.close()
        zfs_send.stdout.close()
        print(f"BPE after {i} bytes transferred")
    close_print_err(zfs_send, "zfs_send")
    close_print_err(zfs_recv, "zfs_recv")
