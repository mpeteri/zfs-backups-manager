import logging
import os
import subprocess
import time
from subprocess import Popen
from typing import List

import click
import psutil

from zbm_types import DRY_RUN, NONINTERACTIVE, DangerLevel

logger: logging.Logger = logging.getLogger(__name__)


def close_print_err(proc: Popen[bytes], name) -> None:
    """
    Wait for process to end
    :raise RuntimeError: when return status is not 0
    """

    logger.debug(f"Waiting for {name} to end. proc.poll() = {proc.poll()}")
    while proc.poll() is None:
        time.sleep(0.01)

    res: bytes = b""
    if proc.stderr is not None:
        for byte in proc.stderr:
            res += byte

    if proc.returncode == -13:
        logger.warning(
            f"{name} returned {proc.returncode} with message {res.decode('utf-8')}"
        )
    elif proc.returncode != 0:
        raise RuntimeError(
            f"{name} returned {proc.returncode} with message {res.decode('utf-8')}"
        )
    elif res != b"":
        logger.info(f"{name} returned message {res.decode('utf-8')}")


def preexec_fn() -> None:
    """Pass this as a hook to Popen to lower ionice priority"""

    os.nice(19)
    p: psutil.Process = psutil.Process(os.getpid())
    p.ionice(psutil.IOPRIO_CLASS_IDLE)


def run(
    cmd: str,
    args: List[str],
    danger_level: DangerLevel = DangerLevel.UNKNOWN,
    no_fail: bool = False,
) -> str:
    if danger_level >= danger_level.SUSPICIOUS:
        logger.warning(cmd + " " + " ".join(args))
    elif danger_level >= danger_level.NORMAL:
        logger.info(cmd + " " + " ".join(args))
    else:
        logger.debug(cmd + " " + " ".join(args))

    if DRY_RUN[0] and danger_level > danger_level.NO_EFFECT:
        raise AssertionError(
            "How did I end up running a command with danger "
            f"level {danger_level} while in a dry run ?"
        )

    result: subprocess.CompletedProcess[bytes] = subprocess.run(
        [cmd] + args, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    res: str = result.stdout.decode("utf-8")
    res_stderr: str = result.stderr.decode("utf-8")
    if result.returncode != 0:
        if no_fail and not NONINTERACTIVE[0]:
            print(
                f"Command {cmd} {' '.join(args)} "
                f"returned with error code {result.returncode} and output:\n{res_stderr}."
            )
            tag = click.prompt(
                "Retry/Skip/Quit", type=click.Choice(["Retry", "Skip", "Quit"])
            )
            if tag == "Retry":
                return run(cmd, args, danger_level=danger_level, no_fail=no_fail)
            elif tag == "Skip":
                return ""
            else:
                os.abort()
        else:
            raise RuntimeError(
                f"Command {cmd} {' '.join(args)} "
                f"returned with error code {result.returncode} and output:\n{res_stderr}."
            )
    return res
