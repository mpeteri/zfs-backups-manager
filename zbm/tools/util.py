from typing import Dict, List

from tools.processes import run
from zbm_types import DangerLevel


def parse_table(command_output: str, header=None) -> List[Dict[str, str]]:
    table: List[List[str]] = [
        x.split("\t") for x in command_output.split("\n") if x.strip() != ""
    ]
    if header is None:
        header = table[0]
        table = table[1:]
    return [{h: v for h, v in zip(header, row)} for row in table]


def send_file(
    source: str, dest_name: str, scp_host: str, remote_metadata_dir: str
) -> None:
    run(
        "scp",
        [source, f"{scp_host}:{remote_metadata_dir}/{dest_name}"],
        danger_level=DangerLevel.NORMAL,
    )
