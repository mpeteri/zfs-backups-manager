import argparse
import datetime
import os
import re
import time
from typing import Dict, List, Optional

import regex

from tools.processes import run


def find_last_log_line(log_extract: str, min_minutes: float) -> str:
    def extract(line: str) -> Optional[datetime.datetime]:
        try:
            rm = regex.match(r"^([A-Za-z]{1,5} +\d{1,2} \d{2}:\d{2}:\d{2}) .*$", line)
            assert rm is not None
            txt: str = rm[1]
        except TypeError:
            return None
        try:
            dt: datetime.datetime = datetime.datetime.strptime(
                f"{datetime.datetime.now().year} {txt}", "%Y %b %d %H:%M:%S"
            )
        except ValueError:
            # Example: time data '2022 NNov  6 14:56:21' does not match format '%Y %b %d %H:%M:%S'
            return None
        return dt

    lines: List[str] = list(log_extract.splitlines())
    try:
        last_date = extract(lines[-1])
        if last_date is None:
            print(f"Warning: could not extract date from last line {lines[-1]}")
        first_date = extract(lines[0])
        if first_date is None:
            print(f"Warning: could not extract date from first line {lines[0]}")
    except IndexError:
        return "No log line extracted"
    if last_date is None or first_date is None:
        return f"Could not extract date from first or last log line. last_date={last_date} and first_date={first_date}"
    out = f"No log line extracted was more than {min_minutes} minutes before {last_date}. First line extracted was on {first_date}."
    for line in log_extract.splitlines():
        dt = extract(line)
        if dt is None:
            continue
        diff = last_date - dt
        if diff.seconds > min_minutes * 60:
            out = line + "\n"
    return out


def test_vm(
    snap: str, vm_names, quiet: bool, metadata_dir: str, max_wait: float
) -> str:
    vm_names[snap] = "unknown"

    print(f"ROLLBACK {snap}")
    run(
        "zfs", ["rollback", "-r", snap]
    )  # -r is only required because of stupid race conditions

    print(f"MOUNTING {snap}")
    parts = regex.match(r"^.*/vm-([0-9]+)-disk-[0-9]+@.*(\d{8}T\d{4}Z?)$", snap)
    assert parts is not None
    vmid: str = parts[1]
    snaptime: str = parts[2]

    config_file = f"{metadata_dir}/{vmid}-{snaptime}.conf"
    if not os.path.exists(config_file):
        print(f"Could not find metadata file {config_file} " "for VM {vmid}, skipping")
        return f"No metadata file {config_file}"

    with open(config_file, "r") as f:
        config: str = f.read()

    vm_names[snap] = re.findall(r"^name: (.*)$", config, flags=re.MULTILINE)[0]
    print(vm_names[snap])

    config = re.sub(
        r"^net0:.*virtio=([A-Fa-f0-9:]+).*$",
        "net0: virtio=\\1,bridge=vmbr1,firewall=1",
        config,
        flags=re.MULTILINE,
    )
    config = re.sub(r"^net[1-9]:.*$", "", config, flags=re.MULTILINE)
    config = re.sub(r"^onboot:.*$", "", config, flags=re.MULTILINE)
    config = re.sub(r"^ide[0-9]+:.*$", "", config, flags=re.MULTILINE)
    config = re.sub(r"^startup:.*$", "", config, flags=re.MULTILINE)
    config = re.sub(r"^cores:.*$", "cores: 1", config, flags=re.MULTILINE)
    config = re.sub(r"^memory:.*$", "memory: 2048", config, flags=re.MULTILINE)
    config = re.sub(
        r"^scsi(\d+): ?[^:]+:vm-(.*)$",
        "scsi\\1: bak-alphabet:vm-\\2",
        config,
        flags=re.MULTILINE,
    )

    print("Unlocking VM 999")
    try:
        run("qm", ["unlock", "999"])
    except RuntimeError:
        print(
            "Could not unlock VM (not locked, or not created). This should not be an issue."
        )

    print("Stopping VM 999")
    try:
        run("qm", ["stop", "999"])
    except RuntimeError:
        print(
            "Could not stop VM (not started, or not created). This should not be an issue."
        )

    with open("/etc/pve/qemu-server/999.conf", "w") as f:
        f.write(config)

    print("Sleeping for 5 seconds")
    time.sleep(5)

    print("Unlocking VM 999")
    try:
        run("qm", ["unlock", "999"])
    except RuntimeError:
        print(
            "Could not unlock VM (not locked, or not created). This should not be an issue."
        )

    print("Sleeping for 5 seconds")
    time.sleep(5)

    print("Starting VM 999")
    run("qm", ["start", "999"])

    ip = f"10.10.10.{vmid}"

    ping: bool = False
    start: float = time.time()
    last_ssh_error: str = ""
    while time.time() < start + max_wait:
        total_wait: float = time.time() - start
        if not ping:
            try:
                res = run("ping", ["-c", "1", ip])
                if not quiet:
                    print("Ping up! Now trying SSH...")
                ping = True
            except RuntimeError:
                if not quiet:
                    print("No ping yet")
        if ping:
            try:
                res = run(
                    "ssh",
                    [
                        "-T",
                        "-i",
                        "/root/backups_manager/backup_check_key",
                        "-o",
                        "BatchMode=yes",
                        "-o",
                        "StrictHostKeyChecking=no",
                        "-o",
                        "ConnectTimeout=5",
                        ip,
                        "tail",
                        "-10000",
                        "/var/log/syslog",
                    ],
                    quiet=True,
                )
                res = find_last_log_line(res, 5 + max_wait / 60)
                print(f"Stopping VM 999")
                run("qm", ["stop", "999"])
                return f"SSH up in {int(total_wait)}s. LAST SYSLOG: {res.strip()}"
            except RuntimeError as e:
                last_ssh_error = str(e)
                if not quiet:
                    print("No SSH yet")
        if not quiet:
            print(
                f"Sleeping for 10 seconds (total wait {int(total_wait)}s / {max_wait}s)"
            )
        time.sleep(10)

    print("SSH timeout")
    print(f"Stopping VM 999")
    run("qm", ["stop", "999"])
    return f"SSH timeout, not booted after {int(time.time() - start)}s. Last SSH error: {last_ssh_error}"


def apply_test_rules(
    snaps: List[str],
    args: argparse.Namespace,
    vm_names: Dict[str, str],
    metadata_dir: str,
    max_wait: float,
) -> Dict[str, str]:
    snaps = list(reversed(sorted(snaps)))
    kept = set()
    n: int = args.test_last
    if n == 0:
        return {}
    for snap in snaps:
        kept.add(snap)
        n -= 1
        if n <= 0:
            break

    test_results = {}
    for snap in list(reversed(sorted(kept))):
        res: str = test_vm(snap, vm_names, args.quiet, metadata_dir, max_wait)
        test_results[snap] = res
    return test_results
