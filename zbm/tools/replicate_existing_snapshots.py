import logging

import regex

from tools.zfs import (list_remote_snapshots, list_snapshots,
                       ordered_snapshot_strings, send_snapshot)
from zbm_types import DRY_RUN, BackupReplicationJob

logger = logging.getLogger(__name__)


def replicate_existing_snapshots(
    job: BackupReplicationJob, local_volume: str, only_last: bool
) -> bool:
    """
    Assumptions:
     - snapshots are created in increasing order lexicographically
    """

    remote_volume: str = regex.sub(
        r"^(.*)/([^/]*)$", f"{job.remote_zfs_root}/\\2", local_volume
    )

    # Get local and remote snap ids
    local_snaps = list_snapshots(local_volume)
    remote_snaps = list_remote_snapshots(
        job.remote_server.ssh_args(), remote_volume, ignore_if_not_exists=True
    )
    local_snap_ids = ordered_snapshot_strings(local_snaps, job.prefix)
    remote_snap_ids = ordered_snapshot_strings(remote_snaps, job.prefix)

    logger.debug(f"Local snapshots: {local_snap_ids}")
    logger.debug(f"Remote snapshots: {remote_snap_ids}")

    common_snap_ids = set(local_snap_ids) & set(remote_snap_ids)
    if len(common_snap_ids) == 0:
        if len(remote_snap_ids):
            logger.warn("No common snapshots ! Skipping")
            return False
        else:
            base_snap_id = None
    else:
        base_snap_id = max(common_snap_ids)
    logger.info(
        f"Selected {base_snap_id} as the first base for incremental replication"
    )

    if only_last:
        if len(local_snap_ids) == 0:
            logger.info("No local snaps")
            return True
        snap_id = max(local_snap_ids)
        if snap_id == base_snap_id:
            logger.info("All snaps are already replicated")
            return True
        logger.info(f"Sending {snap_id} with base {base_snap_id}")
        if DRY_RUN[0]:
            print(f"Would send snap {snap_id} with base {base_snap_id}")
        else:
            send_snapshot(
                f"{local_volume}@{job.prefix}{snap_id}",
                remote_volume,
                None if base_snap_id is None else f"{job.prefix}{base_snap_id}",
                job.remote_server.ssh_args(),
                raw=True,
            )
    else:
        for snap_id in sorted(
            s for s in local_snap_ids if base_snap_id is None or s > base_snap_id
        ):
            logger.info(f"Sending {snap_id} with base {base_snap_id}")
            if DRY_RUN[0]:
                print(f"Would send snap {snap_id} with base {base_snap_id}")
            else:
                send_snapshot(
                    f"{local_volume}@{job.prefix}{snap_id}",
                    remote_volume,
                    None if base_snap_id is None else f"{job.prefix}{base_snap_id}",
                    job.remote_server.ssh_args(),
                    raw=True,
                )
            base_snap_id = snap_id

    return True
