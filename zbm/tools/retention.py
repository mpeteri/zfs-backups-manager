import datetime
from typing import List, Set

import regex


def apply_retention_rules(snaps: List[str], args) -> Set[str]:
    snaps = list(reversed(sorted(snaps)))
    kept: Set[str] = set()

    n: int = args.keep_last
    for snap in snaps:
        kept.add(snap)
        n -= 1
        if n <= 0:
            break

    def update(n, reg) -> None:
        if n == 0:
            return
        if isinstance(reg, str):
            fh = {snap: regex.match(reg, snap)[1] for snap in snaps}
        else:
            fh = {snap: reg(snap) for snap in snaps}
        hs_seen = set()
        for snap in snaps:
            h = fh[snap]
            same_h_kept = {s for s in kept if fh[s] == h}
            if len(same_h_kept) == 0:
                kept.add(snap)
            hs_seen.add(h)
            if len(hs_seen) >= n:
                break

    def snap_to_week(snap):
        h = regex.match(
            r"^.*?(\d{4}\d\d\d\d)T\d\d\d\dZ?$",
            snap,
        )
        assert h is not None
        d = datetime.datetime.strptime(h[1], "%Y%m%d")
        w = f"{d.isocalendar().year}{d.isocalendar().week}"
        return w

    update(args.keep_hourly, r"^.*?(\d{4}\d{2}\d{2}T\d{2})\d{2}Z?$")
    update(args.keep_daily, r"^.*?(\d{4}\d{2}\d{2})T\d{2}\d{2}Z?$")
    update(args.keep_weekly, snap_to_week)
    update(args.keep_monthly, r"^.*?(\d{4}\d{2})\d{2}T\d{2}\d{2}Z?$")
    update(args.keep_yearly, r"^.*?(\d{4})\d{2}\d{2}T\d{2}\d{2}Z?$")

    return kept
