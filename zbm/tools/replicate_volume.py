import datetime
import logging

from regex import regex

from tools.zfs import (create_snapshot, delete_snapshot, list_remote_snapshots,
                       list_snapshots, ordered_snapshot_strings, send_snapshot)
from zbm_types import DRY_RUN, NONINTERACTIVE, VolumeBackupJob

logger = logging.getLogger(__name__)


def backup_volume(job: VolumeBackupJob, local_volume: str) -> str:
    """
    Snapshot the local volume, and send this snapshot to a remote volume.

    Assumptions:
     - the remote volume snapshots are created exclusively using this function.
     - the local volume snapshots with the given prefix are created exclusively
       using this function.
    """

    remote_volume: str = regex.sub(
        r"^(.*)/([^/]*)$", f"{job.remote_zfs_root}/\\2", local_volume
    )

    # Get local and remote snap ids
    local_snaps = list_snapshots(local_volume)
    remote_snaps = list_remote_snapshots(
        job.remote_server.ssh_args(), remote_volume, ignore_if_not_exists=True
    )
    logger.debug(f"Local snapshots: {[a['name'] for a in local_snaps]}")
    logger.debug(f"Remote snapshots: {[a['name'] for a in remote_snaps]}")

    local_snap_ids = ordered_snapshot_strings(local_snaps, job.prefix)
    remote_snap_ids = ordered_snapshot_strings(remote_snaps, job.prefix)

    logger.debug(f"Local snap ids : {local_snap_ids}")
    logger.debug(f"Remote snap ids : {remote_snap_ids}")

    inter = set(local_snap_ids).intersection(set(remote_snap_ids))

    #### CHECK CONSISTENCY ####

    extra_snaps_in_local = set(local_snap_ids) - inter
    if extra_snaps_in_local != set():
        logger.warning(
            f"Found these local snaps that are not present in remote: {extra_snaps_in_local}."
        )
        if DRY_RUN[0]:
            print("Would delete local snapshots {extra_snaps_in_local}")
        else:
            for i in extra_snaps_in_local:
                delete_snapshot(
                    f"{local_volume}@{job.prefix}{i}", force=NONINTERACTIVE[0]
                )
        local_snaps = list_snapshots(local_volume)
        local_snap_ids = ordered_snapshot_strings(local_snaps, job.prefix)

    if len(local_snap_ids) > 0 and local_snap_ids[-1] != remote_snap_ids[-1]:
        raise RuntimeError(
            f"The latest local snap {local_snap_ids[-1]} is "
            "different from the latest remote snap {remote_snap_ids[-1]}. Aborting."
            "\nHint: remove the latest remote snap"
        )

    #### CREATE LOCAL SNAP ####

    new_id = datetime.datetime.utcnow().strftime("%Y%m%dT%H%MZ")
    local_new_snap = f"{local_volume}@{job.prefix}{new_id}"
    if DRY_RUN[0]:
        print(f"Would create snapshot {local_new_snap}")
    else:
        logger.info("Creating snapshot " + local_new_snap)
        create_snapshot(local_new_snap)

    #### IDENTIFY BASE SNAP ####

    base_id = None if local_snap_ids == [] else local_snap_ids[-1]
    if base_id is None:
        local_base_snap = None
        logger.warning(
            f"Cannot find a bak snapshot, will create the first one {job.prefix}{new_id}"
        )
    else:
        local_base_snap = f"{job.prefix}{base_id}"
        logger.info(f"The latest snapshot was {job.prefix}{base_id}")

    #### SEND LOCAL SNAP ####

    if DRY_RUN[0]:
        print(
            f"Would send {local_new_snap} to remote {remote_volume} "
            f"using base {local_base_snap}"
        )
    else:
        if remote_snap_ids == []:
            send_snapshot(
                local_new_snap,
                remote_volume,
                None,
                job.remote_server.ssh_args(),
                raw=job.allow_encrypted,
            )
        else:
            send_snapshot(
                local_new_snap,
                remote_volume,
                local_base_snap,
                job.remote_server.ssh_args(),
                raw=job.allow_encrypted,
            )

    #### CHECK CONSISTENCY ####

    if DRY_RUN[0]:
        print("Would check consistency then exit")
        return ""

    local_snaps = list_snapshots(local_volume)
    remote_snaps = list_remote_snapshots(
        job.remote_server.ssh_args(), remote_volume, ignore_if_not_exists=True
    )
    local_snap_ids = ordered_snapshot_strings(local_snaps, job.prefix)
    remote_snap_ids = ordered_snapshot_strings(remote_snaps, job.prefix)

    if local_snap_ids[-1] == remote_snap_ids[-1]:
        logger.info(
            f"State consistent with the following ids: \n"
            + f"LOCAL {local_snap_ids}\n"
            + f"REMOTE {remote_snap_ids}\n"
        )
    else:
        raise RuntimeError(
            f"Inconsistent state! local {local_snap_ids}, remote {remote_snap_ids}"
        )

    return new_id
