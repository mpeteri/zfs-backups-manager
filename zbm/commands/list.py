import datetime
from dataclasses import dataclass
from typing import Optional

from tools.zfs import (list_remote_snapshots, list_remote_volumes,
                       ordered_snapshot_strings)
from zbm_types import RemoteServer


@dataclass
class ListJob:
    remote_server: RemoteServer
    remote_dataset: Optional[str]
    prefix: Optional[str]


def list_backups(job: ListJob):
    if job.remote_dataset is None:
        print("")
        print("Please specify the dataset with --dataset")
        print("Available datasets: ")
        rv = list_remote_volumes(
            job.remote_server.ssh_args(), "", types={"filesystem", "volume"}
        )
        for v in rv:
            print(f"    {v}")
        print("")
        return

    rs = list_remote_snapshots(job.remote_server.ssh_args(), job.remote_dataset, False)

    if job.prefix is None:
        print("")
        print("Available snapshots: ")
        for v in rs:
            print(f"    {v['name']}")
        print("Hint: specify a backup prefix with --prefix to decode backup dates")
        print("")
        return

    snaps = ordered_snapshot_strings(rs, job.prefix)
    print("")
    print("Available snapshots: ")
    for s in snaps:
        try:
            snap_datetime = datetime.datetime.strptime(s, "%Y%m%dT%H%MZ")
            print(
                f"    {job.remote_dataset}@{job.prefix}{s}"
                f" - {snap_datetime.strftime('%Y-%m-%d %H:%M UTC')}"
            )
        except ValueError:
            snap_datetime = datetime.datetime.strptime(s, "%Y%m%dT%H%M")
            print(
                f"    {job.remote_dataset}@{job.prefix}{s} "
                f"- {snap_datetime.strftime('%Y-%m-%d %H:%M (unknown timezone)')}"
            )
    print("")
