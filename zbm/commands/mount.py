import logging
import os
import random
import signal
import subprocess
import time
from dataclasses import dataclass
from typing import List, Optional

from tools.processes import run
from zbm_types import RemoteServer

logger = logging.getLogger(__name__)


@dataclass
class MountJob:
    remote_server: RemoteServer
    remote_dataset_and_snapshot: str
    keyfile: Optional[str]


def mount(job: MountJob):
    remote_nbd_port = random.randrange(4096, 65000)
    logger.info(f"Selected remote NBD port {remote_nbd_port}")

    pool = job.remote_dataset_and_snapshot.split("/")[0]
    logger.info(f"Identified target zpool {pool}")

    try:
        run(
            "ssh",
            job.remote_server.ssh_args() + ["zfs", "destroy", f"{pool}/tmp_clone"],
        )
    except RuntimeError:
        pass

    try:
        run(
            "ssh",
            job.remote_server.ssh_args()
            + ["zfs", "clone", job.remote_dataset_and_snapshot, f"{pool}/tmp_clone"],
        )
    except RuntimeError as e:
        if "filesystem successfully created, but not mounted" in str(e):
            logger.info(
                "Can't mount the clone remotely because it is encrypted, but "
                "we can still continue."
            )
        else:
            raise e

    fields = ["name"]
    command_output = run(
        "ssh",
        job.remote_server.ssh_args()
        + ["zpool", "list", pool, "-vHP", "-o", ",".join(fields)],
    )
    table: List[List[str]] = [
        x.split("\t") for x in command_output.split("\n") if x.strip() != ""
    ]
    if table[0][0] != pool:
        raise RuntimeError(f"Could not find remote pool {pool} in zpool list")
    disks = [r[1] for r in table[1:] if "/" in r[1]]
    logger.info(f"Identified remote disks {disks}")

    try:
        res = run("nbd-client", ["--version"])
    except FileNotFoundError:
        raise RuntimeError("Please install nbd-client locally first")

    try:
        res = run("zpool", ["--version"])
    except FileNotFoundError:
        raise RuntimeError("Please install zfsutils-linux locally first")

    try:
        res = run("ssh", job.remote_server.ssh_args() + ["nbd-server --version"])
    except RuntimeError:
        raise RuntimeError("Please install nbd-server on the remote first")
    if "version" not in res:
        raise RuntimeError("Please install nbd-server on the remote first")

    # Configure nbd to run with root permissions
    run(
        "ssh",
        job.remote_server.ssh_args()
        + [r"sed -i.bak '/.*\(user\|group\) = /d' /etc/nbd-server/config"],
    )

    tunnel_processes: List[subprocess.Popen[bytes]] = []
    conf = ""
    for i, disk in enumerate(disks):
        logger.info(f"Creating nbd export{i} for disk {disk}")
        conf += f"[export{i}]\nexportname={disk}\nreadonly=true\n\n"

    run(
        "ssh",
        job.remote_server.ssh_args()
        + [f"echo -e '{conf}' > /etc/nbd-server/conf.d/zbm.conf"],
    )
    run("ssh", job.remote_server.ssh_args() + [f"systemctl restart nbd-server"])

    tunnel_args = (
        ["ssh", "-N"] + job.remote_server.ssh_args() + [f"-L10809:127.0.0.1:10809"]
    )
    tunnel_process = subprocess.Popen(
        tunnel_args,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    assert tunnel_process.stdout is not None

    run("modprobe", ["nbd"])

    start_at_nbd = 1

    for i, disk in enumerate(disks):
        logger.info(f"Starting nbd client for disk {disk}")
        run("nbd-client", ["-d", f"/dev/nbd{i + start_at_nbd}"])
        run(
            "nbd-client",
            ["localhost", f"/dev/nbd{i + start_at_nbd}", "-N", f"export{i}"],
        )

    local_pool_alias = f"remote{random.randrange(10000,99999)}"

    os.makedirs("/mnt_remote", exist_ok=True)

    logger.info("Importing pool locally")
    run(
        "zpool",
        [
            "import",
            "-f",
            pool,
            local_pool_alias,
            "-o",
            "readonly=on",
            "-R",
            "/mnt_remote",
            "-N",
        ],
    )

    remote_dataset = job.remote_dataset_and_snapshot.split("@")[0]
    local_dataset_root = (
        local_pool_alias + "/" + ("/".join(remote_dataset.split("/")[1:]))
    )

    if job.keyfile is not None:
        logger.info("Decrypting dataset")
        run(
            "zfs",
            [
                "load-key",
                "-L",
                f"file://{os.path.abspath(job.keyfile)}",
                local_dataset_root,
            ],
        )

    logger.info("Mounting filesystem")
    try:
        run("zfs", ["mount", f"{local_pool_alias}/tmp_clone"])
    except RuntimeError as e:
        print(str(e))
        print(
            "Could not mount clone. This may be normal if the target is a volume and not a filesystem."
        )
        print(
            "In order to mount a LVM volume, you may need to rename your own boot VG and LV, and add the following to /etc/lvm/lvm.conf: "
        )
        print('  	filter = [ "a|/dev/zvol/remote.*/tmp_clone.*|", "r/.*/" ]')
        print("Then run pvscan, vgscan, vgchange -ay")
        print("Interrupt this process to cleanup...")
        signal.sigwait({signal.SIGINT})

        cleanup(
            job,
            tunnel_process,
            disks,
            local_pool_alias,
            remote_nbd_port,
            local_dataset_root,
            pool,
        )
        if "encryption key not loaded" in str(e):
            logger.error("Specify encryption key with --keyfile")
        raise e

    print("Clone mounted at /mnt_remote")
    print("Interrupt this process to unmount...")
    signal.sigwait({signal.SIGINT})

    logger.info("Unounting filesystem")
    run("zfs", ["unmount", f"{local_pool_alias}/tmp_clone"], no_fail=True)

    cleanup(
        job,
        tunnel_process,
        disks,
        local_pool_alias,
        remote_nbd_port,
        local_dataset_root,
        pool,
    )


def cleanup(
    job: MountJob,
    tunnel_process,
    disks,
    local_pool_alias,
    remote_nbd_port,
    local_dataset_root,
    pool,
):
    if job.keyfile is not None:
        logger.info("Unloading key")
        run("zfs", ["unload-key", local_dataset_root])

    logger.info("Exporting pool")
    run("zpool", ["export", local_pool_alias], no_fail=True)

    for i, _ in enumerate(disks):
        run("nbd-client", ["-d", f"/dev/nbd{i}"])

    logger.info("Killing tunnel processes")
    tunnel_process.kill()

    try:
        run("rm", ["--one-file-system", f"/mnt_remote/{local_pool_alias}"])
    except RuntimeError:
        pass

    try:
        run(
            "ssh",
            job.remote_server.ssh_args() + ["zfs", "destroy", f"{pool}/tmp_clone"],
        )
    except RuntimeError:
        pass

    print("Exiting")
