import logging
import os
from dataclasses import dataclass
from typing import List, Optional

import regex

from tools.pruning import keep_n_latest_snapshots
from tools.pve import send_vm_config
from tools.replicate_volume import backup_volume
from tools.zfs import list_remote_volumes, list_volumes
from zbm_types import VolumeBackupJob

logger = logging.getLogger(__name__)


@dataclass
class PVEBackupJob(VolumeBackupJob):
    remote_metadata_dir: str
    include_vms: Optional[List[int]] = None
    exclude_vms: Optional[List[int]] = None


def backup_pve_vms(job: PVEBackupJob):
    logger.info(f"Starting PVE backup job {job}")

    volumes = [v["name"] for v in list_volumes(job.local_zfs_root, types={"volume"})]
    logger.debug("== LOCAL VOLUMES ==")
    logger.debug(volumes)
    logger.debug("")

    try:
        remote_volumes = list_remote_volumes(
            job.remote_server.ssh_args(), job.remote_zfs_root
        )
    except RuntimeError as r:
        if "dataset does not exist" in str(r):
            raise RuntimeError(
                f"Please first create the dataset {job.remote_zfs_root} "
                f"on the remote server.\nHint: zfs create {job.remote_zfs_root}"
            )
        else:
            raise r
    logger.debug("== REMOTE VOLUMES ==")
    logger.debug(remote_volumes)
    logger.debug("")

    volumes_to_backup = [
        v
        for v in volumes
        if regex.match(r"^.*/vm-[0-9]+-disk-[0-9]+$", v)
        and v.startswith(job.local_zfs_root + "/vm-")
        and not v.endswith("swap")
    ]

    logger.info(f"VOLUMES TO BACKUP: {volumes_to_backup}")

    for local_volume in volumes_to_backup:
        vmid: str = regex.sub(r"^.*/vm-([0-9]+)-disk-[0-9]+$", f"\\1", local_volume)

        if job.exclude_vms is not None and int(vmid) in job.exclude_vms:
            logger.debug(f"Skipping excluded VM {vmid}")
            continue

        if job.include_vms is not None and int(vmid) not in job.include_vms:
            logger.debug(f"Skipping non-included VM {vmid}")
            continue

        if not os.path.exists(f"/etc/pve/local/qemu-server/{vmid}.conf"):
            logger.warning(
                f"VM {vmid} does not seem to be local, this must be "
                "a replication volume. Skipping..."
            )
            continue

        backup_id = backup_volume(job, local_volume)
        send_vm_config(vmid, backup_id, job.remote_server, job.remote_metadata_dir)
        keep_n_latest_snapshots(local_volume, job.prefix, 1)
