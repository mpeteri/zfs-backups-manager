import logging
from dataclasses import dataclass
from typing import List, Optional, Set

import regex

from tools.replicate_existing_snapshots import replicate_existing_snapshots
from tools.zfs import list_remote_volumes, list_volumes
from zbm_types import BackupReplicationJob

logger = logging.getLogger(__name__)


def replicate_backups(job: BackupReplicationJob):
    logger.info(f"Starting backup replication job {job}")

    volumes = [v["name"] for v in list_volumes(job.local_zfs_root, job.types)]
    logger.debug("== LOCAL VOLUMES ==")
    logger.debug(volumes)
    logger.debug("")

    volumes_to_backup = volumes

    logger.info(f"VOLUMES TO BACKUP: {volumes_to_backup}")

    for local_volume in volumes_to_backup:
        if job.exclude_vms is not None or job.include_vms is not None:
            vmid: str = regex.sub(r"^.*/vm-([0-9]+)-disk-[0-9]+$", "\\1", local_volume)
            if vmid == local_volume:
                logger.debug(
                    f"Skipping volume {local_volume} that does not seem to belong to a VM"
                )
                continue

            if job.exclude_vms is not None and int(vmid) in job.exclude_vms:
                logger.debug(f"Skipping excluded VM {vmid}")
                continue

            if job.include_vms is not None and int(vmid) not in job.include_vms:
                logger.debug(f"Skipping non-included VM {vmid}")
                continue

        replicate_existing_snapshots(job, local_volume, job.only_last)
